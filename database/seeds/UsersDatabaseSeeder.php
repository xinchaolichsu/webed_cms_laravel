<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersDatabaseSeeder extends Seeder
{
    protected $table = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Register users
         */
        \WebEd\Base\Users\Models\EloquentUser::create([
            'username' => 'webmaster',
            'email' => 'doanquydau@gmail.com',
            'first_name' => 'Gà',
            'last_name' => 'Rừng',
            'display_name' => 'Gà rừng',
            'password' => 'secret',
            'sex' => 'male',
            'status' => 'activated',
            'phone' => '0984848519',
            'mobile_phone' => '0915428202',
            'avatar' => 'http://avatar.nct.nixcdn.com/singer/avatar/2016/01/25/4/1/1/7/1453717802873_600.jpg',
        ]);
        \WebEd\Base\Users\Models\EloquentUser::create([
            'username' => 'administrator',
            'email' => 'doanquydau@outlook.com',
            'first_name' => 'Son',
            'last_name' => 'Goken',
            'display_name' => 'Son Goken',
            'password' => 'secret',
            'sex' => 'other',
            'status' => 'activated',
            'phone' => '0984848519',
            'mobile_phone' => '0915428202',
            'avatar' => 'http://avatar.nct.nixcdn.com/singer/avatar/2016/01/25/4/1/1/7/1453717802873_600.jpg',
        ]);
    }
}
